// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Unreal_gitHUD.generated.h"

UCLASS()
class AUnreal_gitHUD : public AHUD
{
	GENERATED_BODY()

public:
	AUnreal_gitHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

