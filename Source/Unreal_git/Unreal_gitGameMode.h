// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Unreal_gitGameMode.generated.h"

UCLASS(minimalapi)
class AUnreal_gitGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUnreal_gitGameMode();
};



