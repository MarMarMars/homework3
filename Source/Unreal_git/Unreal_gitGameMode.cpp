// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Unreal_gitGameMode.h"
#include "Unreal_gitHUD.h"
#include "Unreal_gitCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUnreal_gitGameMode::AUnreal_gitGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AUnreal_gitHUD::StaticClass();
}
